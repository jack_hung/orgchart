import pytest
import main
import subprocess
import orgchart 

def create_NotJsonfile(emp, org):
    fo = open(emp, "w")
    str = "emp"
    fo.write( str )
    
    fo.close()

    fo = open(org, "w")
    str = "org"
    fo.write( str )
    
    fo.close()

def test_Normalfilepath():

    orgs = orgchart.OrgChart('employees.json', 'organizations.json')
    assert  orgs.checkfile == True

def test_NotcorrectJSON():

    create_NotJsonfile("emp", "org")
    orgs = orgchart.OrgChart('emp', 'org')

    assert  orgs.checkfile == False

def test_NoThisFile():

    orgs = orgchart.OrgChart('qwer', 'asdf')
    assert  orgs.checkfile == False

def test_GetorganizationsFasle():

    orgs = orgchart.OrgChart('employees.json', 'organizations.json')
    org_dev = orgs.organization.get("123")

    assert  org_dev == False

def test_GetorganizationsNormal():

    orgs = orgchart.OrgChart('employees.json', 'organizations.json')
    org_dev = orgs.organization.get(4)

    assert  org_dev == True

def test_GetorganizationsSupervisor():

    orgs = orgchart.OrgChart('employees.json', 'organizations.json')
    org_dev = orgs.organization.get(4)

    test = "<Employee: foo@mail>"
    assert  orgs.organization.supervisor == test

def test_GetorganizationsList():

    orgs = orgchart.OrgChart('employees.json', 'organizations.json')
    org_dev = orgs.organization.list

    test = "<Organization(id=1): President Office>"
    assert  org_dev[0] == test
