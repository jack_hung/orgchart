import json
import argparse
import sys 

def check_OrgInput(args, orgs):
	if args == "supervisor":
		if orgs.organization.supervisor == False:
			print("Can't find supervisor")
		else:
			print("supervisor: {}".format(orgs.organization.supervisor))

	elif args == "employees":
		if orgs.organization.employees == False:
			print("Can't find employee")
		else:
			print("employees: {}".format(orgs.organization.employees))

	elif args == "info":
		if orgs.organization.org_info == False:
			print("Can't find organization information")
		else:
			print("org_info: {}".format(orgs.organization.org_info))

	elif args == "parent":
		if orgs.organization.parent == False:
			print("Can't find parent")
		else:
			print("parent: {}".format(orgs.organization.parent))
		
	elif args == "stop":
		sys.exit("Process stop")


def check_EmpInput(args, orgs):
	if args == "employees_info":
		if orgs.employee.list == False:
			print("Can't find employees")
		else:
			print(json.dumps(orgs.employee.list, indent=4))

	elif args == "supervisor_info":
		if orgs.employee.supervisor_info == False:
			print("Can't find supervisor info")
		else:
			print("Supervisor info: {}".format(orgs.employee.supervisor_info))

	elif args == "organizations":
		if orgs.employee.organizations == False:
			print("Can't find organization information")
		else:
			print("Organizations: {}".format(orgs.employee.organizations))

	elif args == "supervisor_org":
		if orgs.employee.supervisor_organizations == False:
			print("Can't find Supervisor organizations")
		else:
			print("supervisor_organizations: {}".format(orgs.employee.supervisor_organizations))
		
	elif args == "stop":
		sys.exit("Process stop")


class OrgChart:
	def __init__(self, employee_filepath, organization_filepath):
		self.checkfile = self.checkFile(employee_filepath, organization_filepath)
		self.organization = Organization()
		self.employee = Employee()

	def checkFile(self, employee_filepath, organization_filepath):
		global empl_content, orga_content
		try:
			f = open(employee_filepath,'r')
			empl_content = json.loads(f.read())
		except FileNotFoundError:
			print("Error: Read {} failed".format(employee_filepath))
			return False
		except json.decoder.JSONDecodeError:
			print("Error: {} isn't jsonfile".format(employee_filepath))
			f.close()
			return False

		try:
			f = open(organization_filepath, 'r')
			orga_content = json.loads(f.read())
		except FileNotFoundError:
			print("Error: Read {} failed".format(organization_filepath))
			return False
		except json.decoder.JSONDecodeError:
			print("Error: {} isn't jsonfile".format(organization_filepath))
			f.close()
			return False

		return True

class Organization:
	def __init__(self):
		self.list = self.all_org()
		self.supervisor = ""
		self.employees = []
		self.org_info = ""
		self.parent = []

	def all_org(self):
		info = []
		for i in range(len(orga_content)):
			for key, value in orga_content[i].items():
				if (key == "organization_name"):
					info.append("<Organization(id={}): {}>".format(orga_content[i]["id"], value))

		info.sort()
		return info

	def get(self, orgs):
		if type(orgs) == int and orgs <= len(orga_content):
			for i in range(len(orga_content)):
				if (orga_content[i]["id"] == orgs):
					if (orga_content[i]["is_active"]) == False:
						return False
					self.get_supervisor(orga_content[i]["supervisor_id"])
					self.get_employees(orga_content[i]["organization_code"])
					self.get_parent(orga_content[i]["parent_id"])
					self.org_info = self.list[orgs-1]

					return True
		else:
			print("Not find organization id: {}".format(orgs))
			return False

	def get_supervisor(self, var):
		for i in range(len(empl_content)):
			if (empl_content[i]["employee_no"] == var):
				self.supervisor = "<Employee: {}>".format(empl_content[i]["email"])
		
		if self.supervisor == "":
			self.supervisor = False


	def get_employees(self, var):
		for i in range(len(empl_content)):
			if (empl_content[i]["organization_id"] == var):
				self.employees.append("<Employee: {}>".format(empl_content[i]["employee_name"]))

		if self.employees == []:
			self.employees = False

	def get_parent(self, var):
		parent_id = var
		while parent_id != 0:
			for i in range(len(orga_content)):
				if (orga_content[i]["id"] == parent_id):
					self.parent.append("<Organization(id={}): {}>".format(orga_content[i]["id"], orga_content[i]["organization_name"]))
					parent_id = orga_content[i]["parent_id"]

		if self.parent == []:
			self.parent = False

class Employee:
	def __init__(self):
		self.list = []
		self.employee_no = ""
		self.employee_name = ""
		self.email = ""
		self.onboarding_date = ""
		self.organization_id= ""
		self.job_title= ""
		self.gender= ""
		self.supervisor = ""
		self.all_list = self.get_AllList()

		self.applylist = ["employee_no", "employee_name", "email"]
		self.supervisor_info = ""
		self.organizations = []
		self.supervisor_organizations = []

	def get_AllList(self):
		list1 = []

		for i in range(len(empl_content)):
			list1.append("Employee name: {}".format(empl_content[i]["employee_name"]))

		return list1

	def get(self, orgs):
		for i in range(len(empl_content)):
			for key, values in empl_content[i].items():
				if (key not in self.applylist):
					pass
				elif (type(values) == str and values.lower() == orgs.lower()):
					self.list = empl_content[i]
					self.employee_no = empl_content[i]["employee_no"]
					self.employee_name = empl_content[i]["employee_name"]
					self.email = empl_content[i]["email"]
					self.onboarding_date = empl_content[i]["onboarding_date"]
					self.organization_id = empl_content[i]["organization_id"]
					self.job_title = empl_content[i]["job_title"]
					self.gender = empl_content[i]["gender"]

					self.get_supervisor()
					self.get_organization()
					self.get_supervisor_organization()

					return True
		print("Not find employee: {}".format(orgs))
		return False

	def get_supervisor(self):
		for i in range(len(orga_content)):
			if(orga_content[i]["organization_code"] == self.organization_id):
				self.supervisor = orga_content[i]["supervisor_id"]

		for i in range(len(empl_content)):
			if (empl_content[i]["employee_no"] == self.supervisor):
				self.supervisor_info = "<Employee: {}>".format(empl_content[i]["employee_name"])

	def get_organization(self):
		for i in range(len(orga_content)):
			if(orga_content[i]["organization_code"] == self.organization_id):
				self.organizations.append("<Organization(id={}): {}>".format(orga_content[i]["id"], orga_content[i]["organization_name"]))
	
	def get_supervisor_organization(self):
		for i in range(len(orga_content)):
			if(orga_content[i]["supervisor_id"] == self.supervisor ):
				self.supervisor_organizations.append("<Organization(id={}): {}>".format(orga_content[i]["id"], orga_content[i]["organization_name"]))

		for i in range(len(empl_content)):
			if(empl_content[i]["employee_no"] == self.supervisor):
				organization_id = empl_content[i]["organization_id"]
				for i in range(len(orga_content)):
					if(orga_content[i]["organization_code"] == organization_id):
						self.supervisor_organizations.append("<Organization(id={}): {}>".format(orga_content[i]["id"], orga_content[i]["organization_name"]))
