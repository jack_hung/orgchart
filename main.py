import argparse
import sys 
import orgchart 
import json

def usage():
	parser = argparse.ArgumentParser(description='Print organization chart')
	parser.add_argument('-e,', metavar="filepath", dest="employee", type=str, default='employees.json', 
		help='The file path of employees.json (default is current folder)')
	parser.add_argument('-o,', metavar="filepath", dest="organization", type=str, default='organizations.json', 
		help='The file path of organizations.json (default is current folder)')

	return parser

def main():
	parser = usage()
	args = parser.parse_args()

	orgs = orgchart.OrgChart(args.employee, args.organization)

	if orgs.checkfile == False:
		sys.exit()
	
	while True:
		search = input("What you want to search, employee or organization: ")
		if (search == "employee" or search == "organization"):
			break
		else:
			print("Please input employee or organization")
	
	#Organization
	if search == "organization":
		while True:
			org_input = input("Input list or organization id or stop: ")

			try:
				org_dev = orgs.organization.get(int(org_input))
				if org_dev == False:
					print("Organization infomation is not complete")
					continue
				
				while True:
					org_input = input("Input supervisor, employees, info, parent or stop:")

					orgchart.check_OrgInput(org_input, orgs)

			except ValueError:
				
				#Show all organization
				if org_input == "list":
					orgs_list = orgs.organization.list
					for i in orgs_list:
						print(i)
				#Stop
				elif org_input == "stop":
					sys.exit("Process stop")

	#Employee
	if search == "employee":
		while True:
			emp_input = input("Input employee id, name, email, list or stop: ")

			if emp_input == "list":
				for i in orgs.employee.all_list:
						print(i)

			#Stop
			elif emp_input == "stop":
				sys.exit("Process stop")
			
			else:
				if orgs.employee.get(emp_input) == False:
					continue

				while True:
					emp_input = input("Input employees_info, supervisor_info, organizations, supervisor_org or stop:")

					orgchart.check_EmpInput(emp_input, orgs)


if __name__ == '__main__':
	main()